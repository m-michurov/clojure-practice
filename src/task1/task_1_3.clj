(ns task1.task-1-3)

(defn my-reduce
  ([function
    collection]
   (my-reduce function (first collection) (rest collection)))
  ([function
    init
    collection]
   (loop [collection collection
          result init]
     (if (empty? collection)
       result
       (recur
         (rest collection)
         (function result (first collection)))))))

(defn my-map
  [function, collection]
  (my-reduce
    (fn [result, current] (conj result, (function current))),
    [],
    collection))

(defn my-filter
  [predicate, collection]
  (my-reduce
    (fn [result, current]
      (if (predicate current)
        (conj result, current)
        result)),
    [],
    collection))
