(ns task1.task-1-1)
(defn- append-one*
  [element, collections, result]
  (if (empty? collections)
    (reverse result)
    (let [current-collection (conj (first collections), element)]
      (append-one* element, (rest collections), (conj result, current-collection)))))

(defn append-one
  [element, collections]
  (append-one* element, collections, (list)))

(defn- append-all*
  [elements, collections, result]
  (if (empty? elements)
    result,
    (let [current-element (first elements),
          current-collection (append-one current-element, collections)]
      (append-all* (rest elements), collections, (concat result, current-collection)))))

(defn append-all
  [elements, collections]
  (append-all* elements, collections, (list)))

(defn all-words
  [alphabet, n]
  (cond
    (< n 0) (list)
    (= n 0) (list (list))
    :else (append-all alphabet, (all-words alphabet, (- n 1)))))

(defn is-valid-word
  [word]
  (cond
    (<= (count word) 1) true
    (= (first word) (second word)) false
    :else (is-valid-word (rest word))))

(defn- remove-invalid-words*
  [words, result]
  (if (empty? words)
    (reverse result)
    (let [word (first words),
          words (rest words)
          result (if (is-valid-word word) (conj result word) result)]
      (remove-invalid-words* words, result))))

(defn remove-invalid-words
  [words]
  (remove-invalid-words* words, (list)))

(defn words
  [characters, n]
  (remove-invalid-words (all-words characters, n)))
