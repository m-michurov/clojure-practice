(ns task1.task-1-2)

(defn append-one
  ([element, collections]
   (loop [collections collections,
          result []]
     (if (empty? collections)
       result
       (let [collection (conj (first collections), element)]
         (recur (rest collections), (conj result, collection)))))))

(defn append-all
  ([elements, collections]
   (loop [elements elements,
          result []]
     (if (empty? elements)
       result
       (let [element (first elements),
             current-collection (append-one element, collections)]
         (recur (rest elements), (concat result, current-collection)))))))

(defn all-words
  [alphabet, n]
  ((fn [n, result]                                          ; a way to use recur w/out loop
     (if (<= n 0)
       result
       (recur (dec n), (append-all alphabet, result))))
   n
   (list (list))))

(defn is-valid-word
  [word]
  (cond
    (<= (count word) 1) true
    (= (first word) (second word)) false
    :else (recur (rest word))))

(defn remove-invalid-words
  ([words]
   (loop [words words,
          result []]
     (if (empty? words)
       result
       (let [word (first words)]
         (recur
           (rest words),
           (if (is-valid-word word)
             (conj result word)
             result)))))))

(defn words
  [characters, n]
  (remove-invalid-words (all-words characters, n)))
