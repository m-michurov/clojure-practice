(ns task1.task-1-4)

(defn words
  [alphabet, n]
  (if (< n 0)
    (list)
    (letfn [(extend-word [word]
              (map
                (fn [character] (cons character, word)),
                (filter (fn [character] (not= character (first word))), alphabet)))
            (extend-words [words]
              (mapcat extend-word, words))]
      (nth
        (iterate extend-words (list (list)))
        n))))
