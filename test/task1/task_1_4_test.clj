(ns task1.task-1-4-test
  (:require [clojure.test :refer [deftest is testing]],
            [task1.task-1-4 :refer [words]]))

(deftest words-test
  (testing "Empty alphabet"
    (is (=
          (set (words [], 2))
          (set []))))

  (testing "Zero length"
    (is (=
          (set (words [\a, \b, \c], 0))
          (set [[]]))))

  (testing "Negative length"
    (is (=
          (set (words [\a, \b, \c], -1))
          (set []))))

  (testing "Non-empty alphabet, positive length"
    (is (=
          (set (words [\a, \b, \c], 2))
          (set (map seq, ["ab", "ac", "ba", "bc", "ca", "cb"]))))

    (is (=
          (set (words [:a, \b, [2]], 2))
          (set [[:a, \b], [:a, [2]],
                [\b, :a], [\b, [2]],
                [[2], :a], [[2], \b]])))

    (is (=
          (set (words [\a, \b, \c], 3))
          (set (map seq, ["aba", "abc", "aca", "acb", "bab", "bac",
                          "bca", "bcb", "cab", "cac", "cba", "cbc"]))))

    (is (=
          (set (words [\a, \b, \c], 4))
          (set (map seq, ["abab", "abac", "abca", "abcb", "acab", "acac", "acba", "acbc",
                          "baba", "babc", "baca", "bacb", "bcab", "bcac", "bcba", "bcbc",
                          "caba", "cabc", "caca", "cacb", "cbab", "cbac", "cbca", "cbcb"]))))))