(ns task1.task-1-2-test
  (:require [clojure.test :refer [deftest is testing]],
            [task1.task-1-2 :refer [is-valid-word
                                    all-words
                                    words
                                    append-all
                                    append-one
                                    remove-invalid-words]]))

(deftest append-one-test
  (testing "Empty collections list"
    (is (=
          (append-one :a, [])
          [])))

  (testing "Non-empty collections list"
    (is (=
          (append-one :a, [[:b], [[:c]]])
          [[:b, :a], [[:c], :a]]))
    (is (=
          (append-one :a, [[:b], [1], ["abc", 7]])
          [[:b, :a], [1, :a], ["abc", 7, :a]]))))


(deftest append-all-test
  (testing "Empty collections list"
    (testing "empty elements list"
      (is (=
            (append-all [], [])
            [])))

    (testing "non-empty elements list"
      (is (=
            (append-all [:a, :b], [])
            []))))

  (testing "Non-empty collections list"
    (testing "empty elements list"
      (is (=
            (append-all [], [[:c], [1, [2]]])
            [])))

    (testing "non-empty elements list"
      (is (=
            (append-all [:a, :b], [[:c], [1, [2]]])
            [[:c, :a],
             [1, [2], :a],
             [:c, :b],
             [1, [2], :b]])))))


(deftest all-words-test
  (testing "Empty character set"
    (let [result (all-words [], 1)]
      (is (= result []))))

  (testing "Non-empty character set"
    (is (=
          (all-words [:a], 1)
          [[:a]]))

    (is (=
          (all-words [:a], 2)
          [[:a, :a]]))

    (is (=
          (all-words [:a, :b], 1)
          [[:a], [:b]]))

    (is (=
          (set (all-words [:a, :b], 2))
          (set [[:a, :a], [:a, :b], [:b, :a], [:b, :b]]) ))

    (is (=
          (set (all-words [\a, \b], 3))
          (set (map seq, ["aaa", "aab", "aba", "abb", "baa", "bab", "bba", "bbb"])) )))

  (is (=
        (set (all-words [\a, \b, \c], 2))
        (set (map seq, ["aa", "ab", "ac", "ba", "bb", "bc", "ca", "cb", "cc"])) )))

(deftest is-valid-word-test
  (testing "Valid word"
    (testing "single character"
      (is (is-valid-word "a")))

    (testing "multiple characters"
      (is (is-valid-word "ababa"))
      (is (is-valid-word "abc"))))

  (testing "Invalid word"
    (is (not (is-valid-word "aabcd")))
    (is (not (is-valid-word "bcdaa")))
    (is (not (is-valid-word "bcaabc")))))

(deftest remove-invalid-words-test
  (testing "No words"
    (is (= (remove-invalid-words []) [])))

  (testing "All words are valid"
    (is (=
          (remove-invalid-words ["a", "ab", "aba", "abc"])
          ["a", "ab", "aba", "abc"]))

    (is (=
          (remove-invalid-words [[:a], [:a, :b]])
          [[:a], [:a, :b]])))

  (testing "All words are invalid"
    (is (=
          (remove-invalid-words ["aa", "aab", "abb", "abbc"])
          []))

    (is (=
          (remove-invalid-words [[[1], [1]], [:a, :a]])
          [])))

  (testing "Some words are invalid"
    (is (=
          (set (remove-invalid-words ["aa", "aab", "a", "ab", "abb", "abbc", "aba", "abc"]))
          (set ["a", "ab", "aba", "abc"])))

    (is (=
          (set (remove-invalid-words [[:a], "abc", [[1], [1]], [:a, :b], [:a, :a]]))
          (set [[:a], "abc", [:a, :b]])))))

(deftest words-test
  (is (=
        (set (words [\a, \b, \c], 2))
        (set (map seq, ["ab", "ac", "ba", "bc", "ca", "cb"]))))

  (is (=
        (set (words [:a, \b, [2]], 2))
        (set [[:a, \b], [:a, [2]],
              [\b, :a], [\b, [2]],
              [[2], :a], [[2], \b]])))

  (is (=
        (set (words [\a, \b, \c], 3))
        (set (map seq, ["aba", "abc", "aca", "acb", "bab", "bac",
                        "bca", "bcb", "cab", "cac", "cba", "cbc"]))))

  (is (=
        (set (words [\a, \b, \c], 4))
        (set (map seq, ["abab", "abac", "abca", "abcb", "acab", "acac", "acba", "acbc",
                        "baba", "babc", "baca", "bacb", "bcab", "bcac", "bcba", "bcbc",
                        "caba", "cabc", "caca", "cacb", "cbab", "cbac", "cbca", "cbcb"])))))

