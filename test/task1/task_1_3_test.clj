(ns task1.task-1-3-test
  (:require [clojure.test :refer [deftest is testing]],
            [task1.task-1-3 :refer [my-map, my-filter, my-reduce]]))

(deftest my-reduce-test
  (testing "Empty collection"
    (is (= 0 (my-reduce + 0 []))))

  (testing "Non-empty collection"
    (testing "with no init"
      (is (= 6 (my-reduce + [1 2 3]))))

    (testing "with init"
      (is (= 1200 (my-reduce * 10 [1 2 3 4 5]))))))

(deftest my-map-test
  (testing "Empty collection"
    (is (= (map inc, []) (my-map inc, []))))

  (testing "Non-empty collection"
    (let [collection [1, 2, 3]],
      (is (=
            (map inc, collection)
            (my-map inc, collection))))))

(deftest my-filter-test
  (testing "Empty collection"
    (is (= (filter #([%] true), []) (my-filter #([%] true), []))))

  (testing "Non-empty collection"
    (let [collection [1, 2, 3, 4],
          predicate even?]
      (is (=
            (filter predicate, collection),
            (my-filter predicate, collection))))))
