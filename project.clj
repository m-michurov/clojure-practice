(defproject modern-programming-methods-tasks "0.1.0-SNAPSHOT"
  :description "Задания курса \"Современные методы программирования\""
  :url "https://github.com/m-michurov/modern-programming-methods"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]]
  :repl-options {:init-ns modern-programming-methods-tasks.core})
